import { basicLoader } from "@/dataloader/BasicLoader";
import { getDB } from "@/dataloader/db";
import { IRequestContext } from "@/middleware/context";
import DataLoader from "dataloader";
import { Cursor, Db, FilterQuery } from "mongodb";
import { Field, ID, ObjectType } from "type-graphql";
import { v4 } from "uuid";

const removeEmpty = (obj: any) => {
  Object.keys(obj).forEach((key) => obj[key] == null && delete obj[key]);
};

@ObjectType({ description: "Data indexable by UUID" })
export abstract class Indexable<SubType extends Indexable<SubType>> {

  // Makes new object from a common data object
  public static from<T extends Indexable<T>> (
    this: new () => T,
    data: any,
  ): T {
    const obj: T = new this();

    removeEmpty(data);
    Object.assign(obj, data);

    return obj;
  }

  // Creates a new object in mongo database
  public static async create<T extends Indexable<T>> (
    this: new () => T,
    ctx: IRequestContext,
    data: any,
  ): Promise<T> {
    const obj: T = new this();

    removeEmpty(data);
    Object.assign(obj, data);

    obj.created = new Date();
    obj.updated = new Date();

    await obj.save();

    return obj;
  }

  // Loads an object from database given id
  public static async load<T extends Indexable<T>> (
    this: new () => T,
    ctx: IRequestContext,
    id: string,
  ): Promise<T> {
    const obj: T = new this();
    const loader: DataLoader<string, T> = obj.getLoader(ctx);

    return loader.load(id);
  }

  // Retrieves all matching objects
  public static async find<T extends Indexable<T>> (
    this: new () => T,
    ctx: IRequestContext,
    filter: FilterQuery<any>,
    postProc: (cursor: Cursor) => Cursor = (cursor) => cursor,
  ): Promise<(Error | T)[]> {
    const obj: T = new this();
    const loader: DataLoader<string, T> = obj.getLoader(ctx);

    const db: Db = await getDB();
    const collection = db.collection((new this()).getCollection());
    const results: string[] = await postProc(collection
      .find(filter))
      .map((data) => data.id)
      .toArray();

    return loader.loadMany(results);
  }

  @Field((type) => ID, { description: "Object ID" })
  public id: string;

  @Field((type) => Date, { description: "Creation Timestamp" })
  public created: Date;

  @Field((type) => Date, { description: "Update Timestamp" })
  public updated: Date;

  public constructor() {
    this.id = v4();
  }

  // Changes info and updates database
  public async update<T extends Indexable<T>, UpdatingData> (
    ctx: IRequestContext,
    data: UpdatingData,
  ) {
    const loader: DataLoader<string, T> = this.getLoader(ctx);

    removeEmpty(data);
    Object.assign(this, data);

    this.updated = new Date();

    await this.save();

    loader.clear(this.id);
  }

  // Validate object before saving (For special validation rules)
  public validate (obj: Indexable<SubType>) {
    // Blank function
  }

  // Saves object to database
  public async save () {
    this.validate(this);

    const db: Db = await getDB();

    const collection = db.collection(this.getCollection());

    await collection.save(this);
  }

  // Retrieves collection name
  public abstract getCollection (): string;

  // Creates a new loader
  protected makeLoader (c: any): any {
    return basicLoader(c, this.getCollection());
  }

  // Retrieves loader for this type
  private getLoader (ctx: IRequestContext) {
    const collection = this.getCollection();
    const c = this.constructor as SubType extends Indexable<SubType> ? any : any;

    if (!ctx.loaders[collection]) {
      ctx.loaders[collection] = this.makeLoader(c);
    }

    return ctx.loaders[collection];
  }
}
