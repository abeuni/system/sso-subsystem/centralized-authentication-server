import { Field, InputType } from "type-graphql";

@InputType({ description: "Data for creating a greeting" })
export class GreetingCreateInput {
  @Field({ description: "Greeting human-readable ID" })
  public hrid: string;

  @Field({ description: "Greeting template" })
  public template: string;
}
