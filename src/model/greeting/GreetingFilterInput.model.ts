import { Field, InputType } from "type-graphql";

@InputType({ description: "Data for filtering greetings" })
export class GreetingFilterInput {
  @Field({ description: "Greeting human-readable ID", nullable: true })
  public hrid?: string;

  @Field({ description: "Greeting template", nullable: true })
  public template?: string;
}
