import { Field, InputType } from "type-graphql";

@InputType({ description: "Data for updating a greeting" })
export class GreetingUpdateInput {
  @Field({ description: "Greeting human-readable ID", nullable: true })
  public hrid?: string;

  @Field({ description: "Greeting template", nullable: true })
  public template?: string;
}
