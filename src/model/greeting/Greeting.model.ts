import { Indexable } from "@/model/Indexable";
import { Field, ObjectType } from "type-graphql";

@ObjectType({ description: "Represents a greeting" })
export class Greeting extends Indexable<Greeting> {
  @Field({ description: "Greeting human-readable ID" })
  public hrid: string;

  @Field({ description: "Greeting template" })
  public template: string;

  @Field({ description: "Greeting string representation" })
  public representation: string;

  // Required
  public getCollection() {
    return "greetings";
  }
}
