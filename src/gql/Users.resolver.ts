import { Arg, Authorized, Ctx, ID, Mutation, Query, Resolver } from "type-graphql";

import { IRequestContext } from "@/middleware/context";
import cookie, { CookieSerializeOptions } from "cookie";
import jwt, { SignOptions } from "jsonwebtoken";

// Model
import { Role, User } from "@/model/user/User.model";
import { UserCreateInput } from "@/model/user/UserCreateInput.model";
import { ApolloError } from "apollo-server";
import { DateTime, Duration } from "luxon";

@Resolver()
export class UsersResolver {
  @Authorized()
  @Query((returns) => User, {
    description: "Retrieves user with given ID",
  })
  public user (
    @Ctx() ctx: IRequestContext,
    @Arg("id", (type) => ID) id: string,
  ) {
    if (!ctx.roles || (!ctx.roles.includes(Role.ADMIN) && id !== ctx.user)) {
      throw new ApolloError("You don't have permission to access this user's data", "403");
    }
    return User.load(ctx, id);
  }

  @Authorized(Role.ADMIN)
  @Query((returns) => [User], {
    description: "Retrieves list of filtered users",
  })
  public async users (
    @Ctx() ctx: IRequestContext,
  ) {
    return User.find(ctx, {});
  }

  @Authorized(Role.ADMIN)
  @Mutation((returns) => User, {
    description: "Creates a new user",
  })
  public async createUser (
    @Ctx() ctx: IRequestContext,
    @Arg("data") data: UserCreateInput,
  ) {

    if ((await User.find(ctx, { email: data.email })).length > 0)
      throw new ApolloError("User already exists", "409")

    return User.create(ctx, data);
  }

  @Query((returns) => String, {
    description: "Get session token for user",
  })
  public async login (
    @Ctx() ctx: IRequestContext,
    @Arg("email") email: string,
    @Arg("password") password: string,
    @Arg("remember", { nullable: true, defaultValue: false }) remember: boolean,
  ) {
    const users: (Error | User)[] = await User.find(ctx, { email });

    if (users.length > 1) {
      throw new ApolloError("Multiple users found for email", "500");
    }

    if (users[0] instanceof Error) {
      throw new ApolloError("Unknown error", "500")
    }

    const user: User = users[0];

    if (user && user.verifyPassword(password)) {
      const expirationTime: Duration = Duration.fromObject({ week: 1 });

      const jwtOptions: SignOptions = {
        algorithm: "HS256",
        subject: user.id,
      };

      if (!remember) {
        jwtOptions.expiresIn = expirationTime.as("seconds");
      }

      const token = jwt.sign({
        roles: user.roles,
      }, process.env.JWT_SECRET || "JWT_SECRET", jwtOptions);

      const cookieOptions: CookieSerializeOptions = (!remember && {
        expires: DateTime.utc().plus(expirationTime).toJSDate(),
        path: process.env.COOKIE_PATH || "/",
        secure: process.env.NODE_ENV === "development" ? false : true
      }) || {};

      ctx.res.setHeader("Set-Cookie", cookie.serialize("authorization", token, cookieOptions));

      return token;
    }

    throw new ApolloError("Invalid email or password", "401");
  }
}
