import { IRequestContext } from "@/middleware/context";
import { Role, User } from "@/model/user/User.model";
import { AuthChecker } from "type-graphql";

export const authChecker: AuthChecker<IRequestContext> = async (
  { root, args, context, info },
  roles: Role[],
) => {
  // Check if user is logged in
  if (!context.user) {
    return false;
  }

  // If is ADMIN
  if (context.roles && context.roles.includes(Role.ADMIN)) {
    return true;
  }

  // If no roles were given
  if (roles || roles.length === 0) {
    return true;
  }

  // Check if there is role overlap
  return !!roles.find((role) => context.roles.includes(role));
};
