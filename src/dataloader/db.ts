import { logger } from "@/logging";
import { Db, MongoClient } from "mongodb";

let db = null;

export const getDB = async (): Promise<Db> => {
  if (db) {
    return db;
  }

  const uri = process.env.MONGODB_URI || "mongodb://db:27017/";
  const database = process.env.MONGODB_DATA || "database";

  try {
    const client = await MongoClient.connect(uri, {
      useNewUrlParser: true,
    });

    db = client.db(database);

    logger.info(`Connected to '${database}' at '${uri}'`);
  } catch (err) {
    logger.error(`Tried to connect using URI: '${uri}'`, err);
  }

  return db;
};
