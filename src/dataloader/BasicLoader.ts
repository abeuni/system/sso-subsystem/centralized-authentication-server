import { Indexable } from "@/model/Indexable";
import dataloader from "dataloader";
import { Db } from "mongodb";
import { getDB } from "./db";

export function basicLoader<T extends Indexable<T>>(
  model: T extends Indexable<T> ? any : any,
  collection: string,
) {
  return new dataloader<string, T>(async (keys: string[]) => {
    const db: Db = await getDB();

    const raw = await db
      .collection(collection)
      .find({ id: { $in: keys } })
      .toArray();

    const data: T[] = keys.map((key) => (raw.filter((obj) => obj.id === key)[0] || null));

    return data.map((obj) => obj && model.from(obj));
  });
}
